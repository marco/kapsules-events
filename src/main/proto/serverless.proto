// Copyright Kapsules.io (c) 2018. All rights reserved.

syntax = "proto3";

package serverless.core;

option java_package = "io.kapsules.core";
option java_outer_classname = "ServerlessProtos";

// The state of a `Trigger`.
//
enum State {
    ENABLED = 0;
    DISABLED = 1;

    // When in `PAUSED` state, an `Event` may still cause a `Trigger` to
    // be activated, and the event (or `Function` execution) to be queued,
    // but placed "on hold", until such time the `Trigger` is "unpaused"
    // (i.e., it is returned to the `ENABLED` state).
    //
    // Any events/functions queued for a paused trigger will be discarded
    // however, if the trigger is moved to the `DISABLED` state.
    //
    PAUSED = 2;
}

// A `Namespace` defines the scope of the `Event` or `Source` and will be used
// in the determination of which `Function` to execute, as a consequence of a
// `Trigger` to be activated.
//
// The actual syntactical components, and meanings, of the fields of a Namespace are
// left entirely to the implementation and practice of use of the system, and no
// special meaning or format should be inferred from their names; although, it is
// fair to assume that they create somewhat "concentric circles" of influence, in
// which a `domain` contains many `org`s, each one of them may chose to have several
// `scope`s.
//
// For the purposes of the `Serverless` framework, however, we only define an "equality"
// relationship across `namespaces` (two `namespaces` are equal **if and only if** their
// respective components match lexicographically) without any implied ordering.
//
// Components of a `Namespace` must follow DNS components naming, and, in particular,
// Kubernetes's restrictions on their syntax (in particular, no punctuation or special
// characters other than `-`).
// See: https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/
message Namespace {
    string domain = 1;
    string org = 2;
    string scope = 3;
}

// The source of an Event, if applicable.
//
message Source {
    Namespace namespace = 1;
    string name = 2;
}

// The main object that is sent over the Kafka Event stream.
//
// An `event` will signal to the appropriate Consumer that some
// action needs to be taken; or that something noteworthy has
// happened.
//
// The semantics of the `what` happened (and, consequently, or which action(s)
// ought to be taken in response) are obviously left to the consumer itself:
// different consumers will choose to interpret in different ways: a `Scheduler`
// may choose to initiate execution of a `Function`; a `LogAggregator` may just
// make a note of the event; and another consumer, even though subscribed to the
// topic this event was sent to, may choose to drop it silently.
//
// An `Event` belongs to a `namespace` and is (typically, but not necessarily) triggered
// by a `Source` (which may, or may not, belong to the same `Namespace`); further, every
// event has an `eventType` which further defines the semantics of the action to take in
// response; the `eventType` is a **required** field.
//
// Finally, the **optional** `payload` may carry further data and information about the
// event, or may be the object upon which the action must be taken (e.g., the source code
// for a `Function`, that a `build event` will cause to be built and deployed as a Container).
//
// Although there is generally a 1-to-1 relationship between a Kafka topic and the `eventType`,
// this is an "implementation detail" and should not be relied upon: in particular, the mere
// fact that an Event is carried on a particular Topic ought not be used to
// give semantic meaning to the `type` of the event.
//
message Event {
    // Payload serialization format.
    //
    // If `PROTOBUF` is used, the received is expected to infer
    // the Protobuf class from the `Event.eventType`, so that it
    // can correctly deserialize it: no type information is carred
    // along with the `payload`.
    enum SerializedAs {
        JSON = 0;
        PROTOBUF = 1;
        TEXT = 2;
        BYTES = 3;
    }

    // Unique ID of the event (encoded as a UUID)
    string id = 8;

    Namespace namespace = 1;
    string eventType = 2;

    Source source = 3;
    uint64 timestamp = 4;

    // Optional JSON or Protobuf encoded payload.
    bytes payload = 5;
    SerializedAs serializedAs = 6;

    // Instead of carrying the payload, it can be retrieved
    // from the given URI
    string payloadUri = 7;
}

// A Serverless `Function` is uniquely identified by its
// `tag` (which would match a Container image's "tag").
//
// A function belongs to a `Namespace` that also controls its execution,
// scope, and permissions.
//
message Function {
    Namespace ns = 1;
    string tag = 2;
}


// A trigger maps an Event (type) from a given Source
// to a Function with the given `tag`.
//
// If the Trigger is `ENABLED` then the function is invoked, and
// the actual `Event` instance is passed to it (so that the Function may
// gain access to the data in the `payload`, or determine the `eventType`
// that triggered it).
//
// When the `Trigger` is in the `PAUSED` state, invocations may be either
// queued (and delayed until the `Trigger` is re-enabled) or discarded.
//
message Trigger {

    // Unique ID for the trigger, for storage; logging; and event streaming purposes.
    // Deserializes to a UUID.
    //
    string id = 1;

    // An optional human-readable name for the trigger (for, e.g., logging purposes).
    string name = 2;

    // An optional description for the trigger; for, e.g., documentation and
    // logging/reporting purposes.
    string description = 3;


    // This field will be matched to an `Event`'s `source` to determine whether
    // the trigger ought to be activated.
    //
    Source triggerSource = 4;

    State state = 5;

    // Seconds from epoch that mark the (approximate) time this `Trigger`
    // will be "unpaused" and moved back to the `ENABLED` state; if, before this
    // time is reached, the trigger is `DISABLED` the timer is canceled and the
    // trigger will **not** be re-enabled.
    //
    uint64 pausedUntil = 6;

    // This will map the `eventType` of the `Event` that caused this `Trigger`
    // to activate and, if found, will cause the corresponding `Function` to
    // be executed.
    //
    map<string, Function> eventTriggerMapping = 7;
}

message Error {
    int32 code = 1;
    string message = 2;
    string detail = 3;
}
